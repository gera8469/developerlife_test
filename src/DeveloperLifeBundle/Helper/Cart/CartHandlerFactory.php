<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 04.06.17
 * Time: 18:57
 */

namespace DeveloperLifeBundle\Helper\Cart;

use DeveloperLifeBundle\Helper\Cart\Base\AbstractCartHandler;
use DeveloperLifeBundle\Helper\Cart\Exception\CartClassIsNotExistException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CartHandlerFactory
 *
 * Cart handler factory
 *
 * @package DeveloperLifeBundle\Helper\Cart
 */
class CartHandlerFactory
{
    /**
     * Configuration aarray
     *
     * @var array
     */
    private static $config = [];

    /**
     * Create instance of cart handler
     *
     * @throws CartClassIsNotExistException
     * @param string $alias
     * @param string $jsonCartData
     * @param ContainerInterface $container
     * @return AbstractCartHandler
     */
    public static function getCartHandler($alias, $jsonCartData, ContainerInterface $container)
    {
        $cartHandlerClass = self::getHandlerClass($alias);

        $arguments = self::$config['arguments'][$alias];
        $arguments['jsonCartData'] = $jsonCartData;
        $arguments['container'] = $container;

        return call_user_func_array([$cartHandlerClass, 'createHandler'], [$arguments]);
    }

    /**
     * Return cart handler class
     *
     * @param string $alias
     * @throws CartClassIsNotExistException
     * @return string
     */
    private static function getHandlerClass($alias)
    {
        self::initConfig();
        if (empty(self::$config['cartHandlerClass'][$alias])) {
            $cartClassAliasList = implode('", "', array_keys(self::$config['cartHandlerClass']));
            throw new CartClassIsNotExistException("Cart class for alias \"$alias\" is not exists. 
                    You can use \"$cartClassAliasList\".");
        }

        return self::$config['cartHandlerClass'][$alias];
    }

    /**
     * Load config
     *
     * @return array
     */
    private static function initConfig()
    {
        if (empty(self::$config)) {
            self::$config = include __DIR__ . '/config/config.php';
        }

        return self::$config;
    }
}