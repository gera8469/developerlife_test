<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 04.06.17
 * Time: 18:59
 */

/**
 * Config array
 */
return [
    'cartHandlerClass' => [
        'userCart' => \DeveloperLifeBundle\Helper\Cart\Cart\Handler\UserCartHandler::class,
        'groupCart' => \DeveloperLifeBundle\Helper\Cart\Cart\Handler\GroupCartHandler::class,
    ],
    'arguments' => [
        'userCart' => [
        ],
        'groupCart' => [
        ],
    ],

];