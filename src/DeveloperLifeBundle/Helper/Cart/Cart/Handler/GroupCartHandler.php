<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 04.06.17
 * Time: 19:06
 */

namespace DeveloperLifeBundle\Helper\Cart\Cart\Handler;

use DeveloperLifeBundle\Entity\Shop\Group\Cart\GroupCart;
use DeveloperLifeBundle\Entity\Shop\Group\Order\GroupOrder;
use DeveloperLifeBundle\Entity\Shop\Group\Order\GroupOrderHasProduct;
use DeveloperLifeBundle\Entity\Shop\Group\UserGroup;
use DeveloperLifeBundle\Entity\Shop\Group\UserHasGroup;
use DeveloperLifeBundle\Entity\Shop\Shop\Product;
use DeveloperLifeBundle\Entity\Shop\User\Order\OrderStatus;
use DeveloperLifeBundle\Entity\Shop\User\User;
use DeveloperLifeBundle\Helper\Cart\Base\AbstractCartHandler;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class GroupCartHandler
 *
 * @package DeveloperLifeBundle\Helper\Cart\Cart\Handler
 */
class GroupCartHandler extends AbstractCartHandler
{

    /**
     * Full product cart data (with user and product objects)
     *
     * @var array
     */
    private $fullProductCartData = [];

    /**
     * GroupCartHandler constructor.
     *
     * @param string $jsonCartData
     * @param ContainerInterface $container
     */
    public function __construct($jsonCartData, ContainerInterface $container)
    {
        parent::__construct($jsonCartData, $container);

        $this->cartData = $this->getUpdateDCartData();
    }

    /**
     * @param Product $product
     * @param int $quantity
     */
    public function addProduct(Product $product, $quantity = 1)
    {
        $groupCart = $this->getGroupCart($product);

        if (!$groupCart) {
            if ($userHasGroup = $this->getUserHasGroup()) {
                $groupCart = new GroupCart();
                $groupCart->setUserHasGroup($userHasGroup);
                $groupCart->setProduct($product);
                $groupCart->setQuantity($quantity);
            }
        } else {
            $groupCart->setQuantity($groupCart->getQuantity() + $quantity);
        }

        $em = $this->container->get('doctrine')->getManager();
        $em->persist($groupCart);
        $em->flush();
    }

    /**
     * @param Product $product
     * @param int $quantity
     */
    public function removeProduct(Product $product, $quantity = 1)
    {
        $groupCart = $this->getGroupCart($product);
        if ($groupCart) {
            $em = $this->container->get('doctrine')->getManager();

            $oldQuantity = $groupCart->getQuantity();
            $newQuantity = $oldQuantity - $quantity;
            if ($newQuantity < 1) {
                $em->remove($groupCart);
            } else {
                $groupCart->setQuantity($newQuantity);
                $em->persist($groupCart);
            }
            $em->flush();
        }
    }

    /**
     * @param Product $product
     */
    public function removeAllProducts(Product $product)
    {
        $groupCart = $this->getGroupCart($product);
        if ($groupCart) {
            $em = $this->container->get('doctrine')->getManager();
            $em->remove($groupCart);
            $em->flush();
        }
    }

    /**
     * @param null|mixed $orderObj
     */
    public function convertCartToOrder($orderObj = null)
    {
        if (is_null($orderObj) || !is_object($orderObj) || !($orderObj instanceof GroupOrder)) {
            $orderObj = new GroupOrder();
        }

        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getManager();

        $orderObj->setCreationDate(new \DateTime());
        $orderObj->setOrderStatus($doctrine->getRepository(OrderStatus::class)->findOneBy([
            'alias' => 'new',

        ]));
        $orderObj->setUserGroup($this->getUserGroup());
        $orderObj->setNumber('go' . $this->getUserGroup()->getId() . time());
        $em->persist($orderObj);
        $groupCarts = $doctrine->getRepository(GroupCart::class)->findGroupCartsByUserGroup($this->getUserGroup());

        /* @var $groupCart GroupCart*/
        foreach ($groupCarts as $groupCart) {
            $groupOrderHasProduct = new GroupOrderHasProduct();
            $groupOrderHasProduct->setUserHasGroup($groupCart->getUserHasGroup());
            $groupOrderHasProduct->setProduct($groupCart->getProduct());
            $groupOrderHasProduct->setGroupOrder($orderObj);
            $groupOrderHasProduct->setQuantity($groupCart->getQuantity());
            $groupOrderHasProduct->setPrice($groupCart->getProduct()->getPrice());
            $em->persist($groupOrderHasProduct);
            $em->remove($groupCart);
        }
        $em->flush();

    }

    public function clear()
    {
        $doctrine = $this->container->get('doctrine');

        /* @var $userGroup UserGroup */
        $userGroup = $doctrine->getRepository(UserGroup::class)
            ->find($this->cartSettings['activeGroup']);
        /* @var $groupCarts GroupCart[]*/
        $groupCartRepository = $doctrine->getRepository(GroupCart::class);
        $groupCartRepository->clear();
        $groupCarts = $groupCartRepository->findGroupCartsByUserGroup($userGroup);

        $em = $doctrine->getManager();
        foreach ($groupCarts as $groupCart) {
            if ($groupCart->getUserHasGroup()->getUser()->getId() == $this->getUser()->getId()) {
                $em->remove($groupCart);
            }
        }
        $em->flush();
    }

    /**
     * @return bool
     */
    public function isEmpty()
    {
        return empty(count($this->getCartProducts(false)));
    }

    /**
     * Returns full cart data
     *
     * @param boolean $toUpdate
     * @return array
     */
    public function getCartProducts($toUpdate = true)
    {
        if (empty($this->fullProductCartData) || $toUpdate) {

            $fullProductCartData = [];

            $doctrine = $this->container->get('doctrine');

            /* @var $userGroup UserGroup */
            $userGroup = $doctrine->getRepository(UserGroup::class)
                ->find($this->cartSettings['activeGroup']);

            if ($userGroup) {
                /* @var $groupCarts GroupCart[]*/
                $groupCartRepository = $doctrine->getRepository(GroupCart::class);
                $groupCartRepository->clear();
                $groupCarts = $groupCartRepository->findGroupCartsByUserGroup($userGroup);

                foreach ($groupCarts as $groupCart) {
                    $user = $groupCart->getUserHasGroup()->getUser();
                    $product = $groupCart->getProduct();
                    $fullProductCartData[$user->getId()]['user'] = $user;
                    $fullProductCartData[$user->getId()]['products'][$product->getId()]['product'] = $product;
                    $fullProductCartData[$user->getId()]['products'][$product->getId()]['quantity'] = $groupCart->getQuantity();

                }

                foreach ($fullProductCartData as &$userCartData) {
                    $userCartData['summaryData'] = $this->calculateSummaryDataByUser($userCartData);
                }
                $this->fullProductCartData = $fullProductCartData;
            }
        }

        return $this->fullProductCartData;
    }

    /**
     * Check if cart updated
     *
     * @return bool
     */
    public function isCartUpdated()
    {

        $isCartUpdated = false;

        $old = json_encode($this->getUpdateDCartData());

        $counter = 0;
        do {
            sleep(1);
            $new = json_encode($this->getUpdateDCartData());
            if ($old != $new) {
                $isCartUpdated = true;
                break;
            }
            $counter++;
        } while ($counter <= 25);

        return $isCartUpdated;
    }

    /**
     * Return cart data
     *
     * @param bool $toUpdate
     * @return array
     */
    public function getCartData($toUpdate = true)
    {
        if ($toUpdate) {
            return $this->getUpdateDCartData();
        }
        return parent::getCartData();
    }

    /**
     * Get cart data
     *
     * @return array
     */
    private function getUpdateDCartData()
    {
        $cartData = [
            'activeGroup' => $this->cartSettings['activeGroup'],
            'usersData' => $this->getSimpleCartProducts(),
            'summaryCartData' => $this->calculateSummaryDataByCart($this->getCartProducts(false)),
        ];
        return $cartData;
    }

    /**
     * Return simple cart products (without objects)
     *
     * @return array
     */
    private function getSimpleCartProducts()
    {
        $cartProducts = $this->getCartProducts();

        $simpleCartProducts = [];

        foreach ($cartProducts as $userId => $cartData) {
            $simpleCartProducts[$userId] = [
                'products' => [],
                'summaryData' => $cartData['summaryData'],
            ];

            foreach ($cartData['products'] as $productId => $productData) {
                $simpleCartProducts[$userId]['products'][$productId] = [
                    'quantity' => $productData['quantity']
                ];
            }
        }

        return $simpleCartProducts;
    }

    /**
     * Calculate summary data (summary cost and summary quantity)
     *
     * @param array $fullCartData
     * @return array
     */
    private function calculateSummaryDataByCart(array $fullCartData)
    {
        $summaryCartData = [
            'summaryCartCost' => 0,
            'summaryCartQuantity' => 0,
        ];

        foreach ($fullCartData as $userData) {
            $summaryCartData['summaryCartCost'] += $userData['summaryData']['summaryCost'];
            $summaryCartData['summaryCartQuantity'] += $userData['summaryData']['summaryQuantity'];
        }

        return $summaryCartData;
    }

    /**
     * Calculate summary data by user (summary cost and summary quantity)
     *
     * @param array $userData
     * @return array
     */
    private function calculateSummaryDataByUser(array $userData)
    {
        $summaryData = [
            'summaryCost' => 0,
            'summaryQuantity' => 0,
        ];

        foreach ($userData['products'] as $productData) {
            $summaryData['summaryCost'] += $productData['product']->getPrice() * $productData['quantity'];
            $summaryData['summaryQuantity'] += $productData['quantity'];
        }

        return $summaryData;
    }

    /**
     * Return GroupCart by cart settings
     *
     * @param Product $product
     * @return null|GroupCart
     */
    private function getGroupCart(Product $product)
    {
        return $this->container->get('doctrine')
            ->getRepository(GroupCart::class)->findOneBy([
                'userHasGroup' => $this->getUserHasGroup(),
                'product' => $product,
            ]);
    }

    /**
     * Return UserHasGroup
     *
     * @return UserHasGroup|null
     */
    private function getUserHasGroup()
    {
        return $this->container->get('doctrine')
            ->getRepository(UserHasGroup::class)->findOneBy([
                'user' => $this->getUser(),
                'userGroup' => $this->getUserGroup()
            ]);
    }

    /**
     * Return UserGroup
     *
     * @return UserGroup|null
     */
    private function getUserGroup()
    {
        return $this->container->get('doctrine')
            ->getRepository(UserGroup::class)->find($this->cartData['activeGroup']);
    }

    /**
     * Return User
     *
     * @return User|null
     */
    private function getUser(){

        return $this->container->get('security.token_storage')->getToken()->getUser();
    }
}