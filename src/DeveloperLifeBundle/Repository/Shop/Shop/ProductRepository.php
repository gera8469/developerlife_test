<?php

namespace DeveloperLifeBundle\Repository\Shop\Shop;

use DeveloperLifeBundle\Entity\Shop\Shop\Product;
use Doctrine\ORM\EntityRepository;

/**
 * ProductRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ProductRepository extends EntityRepository
{
    /**
     * Find products by id list
     *
     * @param array $idList
     * @return Product[]
     */
    public function findByIdList(array $idList)
    {

        $queryBuilder = $this->createQueryBuilder('pr');
        $queryBuilder->select('pr')->add('where', $queryBuilder->expr()->in('pr.id', ':idList'));
        $queryBuilder->setParameter('idList', $idList);

        return $queryBuilder->getQuery()->getResult();
    }
}
