<?php

namespace DeveloperLifeBundle\Repository\Shop\Group\Order;

use Doctrine\ORM\EntityRepository;

/**
 * GroupOrderHasProductRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class GroupOrderHasProductRepository extends EntityRepository
{
}
