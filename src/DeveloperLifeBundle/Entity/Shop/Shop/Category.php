<?php

namespace DeveloperLifeBundle\Entity\Shop\Shop;

use Doctrine\ORM\Mapping as ORM;

/**
 * Category
 */
class Category
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $image;

    /**
     * @var bool
     */
    private $isDeleted = false;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Category
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Category
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     * @return Category
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean 
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $products;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $childCategories;

    /**
     * @var \DeveloperLifeBundle\Entity\Shop\Shop\Category
     */
    private $parentCategory;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->products = new \Doctrine\Common\Collections\ArrayCollection();
        $this->childCategories = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add products
     *
     * @param \DeveloperLifeBundle\Entity\Shop\Shop\Product $products
     * @return Category
     */
    public function addProduct(\DeveloperLifeBundle\Entity\Shop\Shop\Product $products)
    {
        $this->products[] = $products;

        return $this;
    }

    /**
     * Remove products
     *
     * @param \DeveloperLifeBundle\Entity\Shop\Shop\Product $products
     */
    public function removeProduct(\DeveloperLifeBundle\Entity\Shop\Shop\Product $products)
    {
        $this->products->removeElement($products);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Add childCategories
     *
     * @param \DeveloperLifeBundle\Entity\Shop\Shop\Category $childCategories
     * @return Category
     */
    public function addChildCategory(\DeveloperLifeBundle\Entity\Shop\Shop\Category $childCategories)
    {
        $this->childCategories[] = $childCategories;

        return $this;
    }

    /**
     * Remove childCategories
     *
     * @param \DeveloperLifeBundle\Entity\Shop\Shop\Category $childCategories
     */
    public function removeChildCategory(\DeveloperLifeBundle\Entity\Shop\Shop\Category $childCategories)
    {
        $this->childCategories->removeElement($childCategories);
    }

    /**
     * Get childCategories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getChildCategories()
    {
        return $this->childCategories;
    }

    /**
     * Set parentCategory
     *
     * @param \DeveloperLifeBundle\Entity\Shop\Shop\Category $parentCategory
     * @return Category
     */
    public function setParentCategory(\DeveloperLifeBundle\Entity\Shop\Shop\Category $parentCategory = null)
    {
        $this->parentCategory = $parentCategory;

        return $this;
    }

    /**
     * Get parentCategory
     *
     * @return \DeveloperLifeBundle\Entity\Shop\Shop\Category 
     */
    public function getParentCategory()
    {
        return $this->parentCategory;
    }
}
