<?php

namespace DeveloperLifeBundle\Entity\Shop\User\Order;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserOrderHasProduct
 */
class UserOrderHasProduct
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $quantity;

    /**
     * @var float
     */
    private $price;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return UserOrderHasProduct
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return UserOrderHasProduct
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }
    /**
     * @var \DeveloperLifeBundle\Entity\Shop\User\Order\UserOrder
     */
    private $userOrder;

    /**
     * @var \DeveloperLifeBundle\Entity\Shop\Shop\Product
     */
    private $product;


    /**
     * Set userOrder
     *
     * @param \DeveloperLifeBundle\Entity\Shop\User\Order\UserOrder $userOrder
     * @return UserOrderHasProduct
     */
    public function setUserOrder(\DeveloperLifeBundle\Entity\Shop\User\Order\UserOrder $userOrder = null)
    {
        $this->userOrder = $userOrder;

        return $this;
    }

    /**
     * Get userOrder
     *
     * @return \DeveloperLifeBundle\Entity\Shop\User\Order\UserOrder 
     */
    public function getUserOrder()
    {
        return $this->userOrder;
    }

    /**
     * Set product
     *
     * @param \DeveloperLifeBundle\Entity\Shop\Shop\Product $product
     * @return UserOrderHasProduct
     */
    public function setProduct(\DeveloperLifeBundle\Entity\Shop\Shop\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \DeveloperLifeBundle\Entity\Shop\Shop\Product 
     */
    public function getProduct()
    {
        return $this->product;
    }
}
