<?php

namespace DeveloperLifeBundle\Entity\Shop\User\Order;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderStatus
 */
class OrderStatus
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $alias;

    /**
     * @var string
     */
    private $description;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return OrderStatus
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return OrderStatus
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $userOrders;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $groupOrders;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->userOrders = new \Doctrine\Common\Collections\ArrayCollection();
        $this->groupOrders = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add userOrders
     *
     * @param \DeveloperLifeBundle\Entity\Shop\User\Order\UserOrder $userOrders
     * @return OrderStatus
     */
    public function addUserOrder(\DeveloperLifeBundle\Entity\Shop\User\Order\UserOrder $userOrders)
    {
        $this->userOrders[] = $userOrders;

        return $this;
    }

    /**
     * Remove userOrders
     *
     * @param \DeveloperLifeBundle\Entity\Shop\User\Order\UserOrder $userOrders
     */
    public function removeUserOrder(\DeveloperLifeBundle\Entity\Shop\User\Order\UserOrder $userOrders)
    {
        $this->userOrders->removeElement($userOrders);
    }

    /**
     * Get userOrders
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUserOrders()
    {
        return $this->userOrders;
    }

    /**
     * Add groupOrders
     *
     * @param \DeveloperLifeBundle\Entity\Shop\Group\Order\GroupOrder $groupOrders
     * @return OrderStatus
     */
    public function addGroupOrder(\DeveloperLifeBundle\Entity\Shop\Group\Order\GroupOrder $groupOrders)
    {
        $this->groupOrders[] = $groupOrders;

        return $this;
    }

    /**
     * Remove groupOrders
     *
     * @param \DeveloperLifeBundle\Entity\Shop\Group\Order\GroupOrder $groupOrders
     */
    public function removeGroupOrder(\DeveloperLifeBundle\Entity\Shop\Group\Order\GroupOrder $groupOrders)
    {
        $this->groupOrders->removeElement($groupOrders);
    }

    /**
     * Get groupOrders
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGroupOrders()
    {
        return $this->groupOrders;
    }



    /**
     * Set alias
     *
     * @param string $alias
     *
     * @return OrderStatus
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }
}
