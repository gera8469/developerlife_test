<?php

namespace DeveloperLifeBundle\Entity\Shop\User\Order;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserOrder
 */
class UserOrder
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $number;

    /**
     * @var \DateTime
     */
    private $creationDate;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param string $number
     * @return UserOrder
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return UserOrder
     */
    public function setCreationDate(\DateTime $creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $userOrderHasProducts;

    /**
     * @var \DeveloperLifeBundle\Entity\Shop\User\User
     */
    private $user;

    /**
     * @var \DeveloperLifeBundle\Entity\Shop\User\DeliveryAddress
     */
    private $deliveryAddress;

    /**
     * @var \DeveloperLifeBundle\Entity\Shop\User\Order\OrderStatus
     */
    private $orderStatus;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->userOrderHasProducts = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add userOrderHasProducts
     *
     * @param \DeveloperLifeBundle\Entity\Shop\User\Order\UserOrderHasProduct $userOrderHasProducts
     * @return UserOrder
     */
    public function addUserOrderHasProduct(\DeveloperLifeBundle\Entity\Shop\User\Order\UserOrderHasProduct $userOrderHasProducts)
    {
        $this->userOrderHasProducts[] = $userOrderHasProducts;

        return $this;
    }

    /**
     * Remove userOrderHasProducts
     *
     * @param \DeveloperLifeBundle\Entity\Shop\User\Order\UserOrderHasProduct $userOrderHasProducts
     */
    public function removeUserOrderHasProduct(\DeveloperLifeBundle\Entity\Shop\User\Order\UserOrderHasProduct $userOrderHasProducts)
    {
        $this->userOrderHasProducts->removeElement($userOrderHasProducts);
    }

    /**
     * Get userOrderHasProducts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUserOrderHasProducts()
    {
        return $this->userOrderHasProducts;
    }

    /**
     * Set user
     *
     * @param \DeveloperLifeBundle\Entity\Shop\User\User $user
     * @return UserOrder
     */
    public function setUser(\DeveloperLifeBundle\Entity\Shop\User\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \DeveloperLifeBundle\Entity\Shop\User\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set deliveryAddress
     *
     * @param \DeveloperLifeBundle\Entity\Shop\User\DeliveryAddress $deliveryAddress
     * @return UserOrder
     */
    public function setDeliveryAddress(\DeveloperLifeBundle\Entity\Shop\User\DeliveryAddress $deliveryAddress = null)
    {
        $this->deliveryAddress = $deliveryAddress;

        return $this;
    }

    /**
     * Get deliveryAddress
     *
     * @return \DeveloperLifeBundle\Entity\Shop\User\DeliveryAddress 
     */
    public function getDeliveryAddress()
    {
        return $this->deliveryAddress;
    }

    /**
     * Set orderStatus
     *
     * @param \DeveloperLifeBundle\Entity\Shop\User\Order\OrderStatus $orderStatus
     * @return UserOrder
     */
    public function setOrderStatus(\DeveloperLifeBundle\Entity\Shop\User\Order\OrderStatus $orderStatus = null)
    {
        $this->orderStatus = $orderStatus;

        return $this;
    }

    /**
     * Get orderStatus
     *
     * @return \DeveloperLifeBundle\Entity\Shop\User\Order\OrderStatus 
     */
    public function getOrderStatus()
    {
        return $this->orderStatus;
    }

    public function getStringCreationDate($format = 'd.m.y - H:i:s')
    {
        return !empty($this->creationDate) ? $this->creationDate->format($format) : '';
    }

    public function getSummaryData()
    {
        /* @var $orderHasProducts UserOrderHasProduct[]*/
        $orderHasProducts = $this->getUserOrderHasProducts();

        $summaryData = [
            'summaryQuantity' => 0,
            'summaryCost' => 0,

        ];

        foreach ($orderHasProducts as $orderHasProduct) {
            $summaryData['summaryQuantity'] += $orderHasProduct->getQuantity();
            $summaryData['summaryCost'] += $orderHasProduct->getPrice();

        }

        return $summaryData;
    }
}
