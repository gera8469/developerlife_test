<?php

namespace DeveloperLifeBundle\Entity\Shop\Group\Order;

use Doctrine\ORM\Mapping as ORM;

/**
 * GroupOrderHasProduct
 */
class GroupOrderHasProduct
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $quantity;

    /**
     * @var float
     */
    private $price;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return GroupOrderHasProduct
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return GroupOrderHasProduct
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }
    /**
     * @var \DeveloperLifeBundle\Entity\Shop\Group\UserHasGroup
     */
    private $userHasGroup;

    /**
     * @var \DeveloperLifeBundle\Entity\Shop\Shop\Product
     */
    private $product;

    /**
     * @var \DeveloperLifeBundle\Entity\Shop\Group\Order\GroupOrder
     */
    private $groupOrder;


    /**
     * Set userHasGroup
     *
     * @param \DeveloperLifeBundle\Entity\Shop\Group\UserHasGroup $userHasGroup
     * @return GroupOrderHasProduct
     */
    public function setUserHasGroup(\DeveloperLifeBundle\Entity\Shop\Group\UserHasGroup $userHasGroup = null)
    {
        $this->userHasGroup = $userHasGroup;

        return $this;
    }

    /**
     * Get userHasGroup
     *
     * @return \DeveloperLifeBundle\Entity\Shop\Group\UserHasGroup 
     */
    public function getUserHasGroup()
    {
        return $this->userHasGroup;
    }

    /**
     * Set product
     *
     * @param \DeveloperLifeBundle\Entity\Shop\Shop\Product $product
     * @return GroupOrderHasProduct
     */
    public function setProduct(\DeveloperLifeBundle\Entity\Shop\Shop\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \DeveloperLifeBundle\Entity\Shop\Shop\Product 
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set groupOrder
     *
     * @param \DeveloperLifeBundle\Entity\Shop\Group\Order\GroupOrder $groupOrder
     * @return GroupOrderHasProduct
     */
    public function setGroupOrder(\DeveloperLifeBundle\Entity\Shop\Group\Order\GroupOrder $groupOrder = null)
    {
        $this->groupOrder = $groupOrder;

        return $this;
    }

    /**
     * Get groupOrder
     *
     * @return \DeveloperLifeBundle\Entity\Shop\Group\Order\GroupOrder 
     */
    public function getGroupOrder()
    {
        return $this->groupOrder;
    }
}
