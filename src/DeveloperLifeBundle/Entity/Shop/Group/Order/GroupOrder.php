<?php

namespace DeveloperLifeBundle\Entity\Shop\Group\Order;

use Doctrine\ORM\Mapping as ORM;

/**
 * GroupOrder
 */
class GroupOrder
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $number;

    /**
     * @var \DateTime
     */
    private $creationDate;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param string $number
     * @return GroupOrder
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return GroupOrder
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $groupOrderHasProducts;

    /**
     * @var \DeveloperLifeBundle\Entity\Shop\User\Order\OrderStatus
     */
    private $orderStatus;

    /**
     * @var \DeveloperLifeBundle\Entity\Shop\Group\UserGroup
     */
    private $userGroup;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->groupOrderHasProducts = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add groupOrderHasProducts
     *
     * @param \DeveloperLifeBundle\Entity\Shop\Group\Order\GroupOrderHasProduct $groupOrderHasProducts
     * @return GroupOrder
     */
    public function addGroupOrderHasProduct(\DeveloperLifeBundle\Entity\Shop\Group\Order\GroupOrderHasProduct $groupOrderHasProducts)
    {
        $this->groupOrderHasProducts[] = $groupOrderHasProducts;

        return $this;
    }

    /**
     * Remove groupOrderHasProducts
     *
     * @param \DeveloperLifeBundle\Entity\Shop\Group\Order\GroupOrderHasProduct $groupOrderHasProducts
     */
    public function removeGroupOrderHasProduct(\DeveloperLifeBundle\Entity\Shop\Group\Order\GroupOrderHasProduct $groupOrderHasProducts)
    {
        $this->groupOrderHasProducts->removeElement($groupOrderHasProducts);
    }

    /**
     * Get groupOrderHasProducts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGroupOrderHasProducts()
    {
        return $this->groupOrderHasProducts;
    }

    /**
     * Set orderStatus
     *
     * @param \DeveloperLifeBundle\Entity\Shop\User\Order\OrderStatus $orderStatus
     * @return GroupOrder
     */
    public function setOrderStatus(\DeveloperLifeBundle\Entity\Shop\User\Order\OrderStatus $orderStatus = null)
    {
        $this->orderStatus = $orderStatus;

        return $this;
    }

    /**
     * Get orderStatus
     *
     * @return \DeveloperLifeBundle\Entity\Shop\User\Order\OrderStatus 
     */
    public function getOrderStatus()
    {
        return $this->orderStatus;
    }

    /**
     * Set userGroup
     *
     * @param \DeveloperLifeBundle\Entity\Shop\Group\UserGroup $userGroup
     * @return GroupOrder
     */
    public function setUserGroup(\DeveloperLifeBundle\Entity\Shop\Group\UserGroup $userGroup = null)
    {
        $this->userGroup = $userGroup;

        return $this;
    }

    /**
     * Get userGroup
     *
     * @return \DeveloperLifeBundle\Entity\Shop\Group\UserGroup 
     */
    public function getUserGroup()
    {
        return $this->userGroup;
    }
    /**
     * @var \DeveloperLifeBundle\Entity\Shop\User\DeliveryAddress
     */
    private $deliveryAddress;


    /**
     * Set deliveryAddress
     *
     * @param \DeveloperLifeBundle\Entity\Shop\User\DeliveryAddress $deliveryAddress
     *
     * @return GroupOrder
     */
    public function setDeliveryAddress(\DeveloperLifeBundle\Entity\Shop\User\DeliveryAddress $deliveryAddress = null)
    {
        $this->deliveryAddress = $deliveryAddress;

        return $this;
    }

    /**
     * Get deliveryAddress
     *
     * @return \DeveloperLifeBundle\Entity\Shop\User\DeliveryAddress
     */
    public function getDeliveryAddress()
    {
        return $this->deliveryAddress;
    }

    public function getStringCreationDate($format = 'd.m.y - H:i:s')
    {
        return !empty($this->creationDate) ? $this->creationDate->format($format) : '';
    }

    public function getSummaryData()
    {
        /* @var $orderHasProducts GroupOrderHasProduct[]*/
        $orderHasProducts = $this->getGroupOrderHasProducts();

        $summaryData = [
            'summaryQuantity' => 0,
            'summaryCost' => 0,

        ];

        foreach ($orderHasProducts as $orderHasProduct) {
            $summaryData['summaryQuantity'] += $orderHasProduct->getQuantity();
            $summaryData['summaryCost'] += $orderHasProduct->getPrice();

        }

        return $summaryData;
    }
}
