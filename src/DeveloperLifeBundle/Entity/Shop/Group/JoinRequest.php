<?php

namespace DeveloperLifeBundle\Entity\Shop\Group;

/**
 * JoinRequest
 */
class JoinRequest
{
    /**
     * @var int
     */
    private $id;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @var \DeveloperLifeBundle\Entity\Shop\User\User
     */
    private $user;

    /**
     * @var \DeveloperLifeBundle\Entity\Shop\Group\UserGroup
     */
    private $userGroup;


    /**
     * Set user
     *
     * @param \DeveloperLifeBundle\Entity\Shop\User\User $user
     *
     * @return JoinRequest
     */
    public function setUser(\DeveloperLifeBundle\Entity\Shop\User\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \DeveloperLifeBundle\Entity\Shop\User\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set userGroup
     *
     * @param \DeveloperLifeBundle\Entity\Shop\Group\UserGroup $userGroup
     *
     * @return JoinRequest
     */
    public function setUserGroup(\DeveloperLifeBundle\Entity\Shop\Group\UserGroup $userGroup = null)
    {
        $this->userGroup = $userGroup;

        return $this;
    }

    /**
     * Get userGroup
     *
     * @return \DeveloperLifeBundle\Entity\Shop\Group\UserGroup
     */
    public function getUserGroup()
    {
        return $this->userGroup;
    }
}
