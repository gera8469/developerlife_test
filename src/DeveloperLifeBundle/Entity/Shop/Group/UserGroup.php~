<?php

namespace DeveloperLifeBundle\Entity\Shop\Group;

use DeveloperLifeBundle\Entity\Shop\Group\Order\GroupOrder;
use Doctrine\ORM\Mapping as ORM;

/**
 * UserGroup
 */
class UserGroup
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return UserGroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return UserGroup
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $groupHasUsers;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $groupOrders;

    /**
     * @var \DeveloperLifeBundle\Entity\Shop\User\User
     */
    private $owner;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->groupHasUsers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->groupOrders = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add groupHasUsers
     *
     * @param \DeveloperLifeBundle\Entity\Shop\Group\UserHasGroup $groupHasUsers
     * @return UserGroup
     */
    public function addGroupHasUser(\DeveloperLifeBundle\Entity\Shop\Group\UserHasGroup $groupHasUsers)
    {
        $this->groupHasUsers[] = $groupHasUsers;

        return $this;
    }

    /**
     * Remove groupHasUsers
     *
     * @param \DeveloperLifeBundle\Entity\Shop\Group\UserHasGroup $groupHasUsers
     */
    public function removeGroupHasUser(\DeveloperLifeBundle\Entity\Shop\Group\UserHasGroup $groupHasUsers)
    {
        $this->groupHasUsers->removeElement($groupHasUsers);
    }

    /**
     * Get groupHasUsers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGroupHasUsers()
    {
        return $this->groupHasUsers;
    }

    /**
     * Add groupOrders
     *
     * @param GroupOrder $groupOrders
     * @return UserGroup
     */
    public function addGroupOrder(GroupOrder $groupOrders)
    {
        $this->groupOrders[] = $groupOrders;

        return $this;
    }

    /**
     * Remove groupOrders
     *
     * @param GroupOrder $groupOrders
     */
    public function removeGroupOrder(GroupOrder $groupOrders)
    {
        $this->groupOrders->removeElement($groupOrders);
    }

    /**
     * Get groupOrders
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGroupOrders()
    {
        return $this->groupOrders;
    }

    /**
     * Set owner
     *
     * @param \DeveloperLifeBundle\Entity\Shop\User\User $owner
     * @return UserGroup
     */
    public function setOwner(\DeveloperLifeBundle\Entity\Shop\User\User $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \DeveloperLifeBundle\Entity\Shop\User\User 
     */
    public function getOwner()
    {
        return $this->owner;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $joinRequests;


    /**
     * Add joinRequest
     *
     * @param \DeveloperLifeBundle\Entity\Shop\Group\JoinRequest $joinRequest
     *
     * @return UserGroup
     */
    public function addJoinRequest(\DeveloperLifeBundle\Entity\Shop\Group\JoinRequest $joinRequest)
    {
        $this->joinRequests[] = $joinRequest;

        return $this;
    }

    /**
     * Remove joinRequest
     *
     * @param \DeveloperLifeBundle\Entity\Shop\Group\JoinRequest $joinRequest
     */
    public function removeJoinRequest(\DeveloperLifeBundle\Entity\Shop\Group\JoinRequest $joinRequest)
    {
        $this->joinRequests->removeElement($joinRequest);
    }

    /**
     * Get joinRequest
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getJoinRequests()
    {
        return $this->joinRequests;
    }
}
