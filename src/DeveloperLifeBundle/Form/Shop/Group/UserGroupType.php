<?php

namespace DeveloperLifeBundle\Form\Shop\Group;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UserGroupType
 *
 * @package DeveloperLifeBundle\Form\Shop\Group
 */
class UserGroupType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'translation_domain' => 'profile',
                'label' => 'userGroup.form.labelLink.name',
            ])
            ->add('description', TextareaType::class, [
                'translation_domain' => 'profile',
                'label' => 'userGroup.form.labelLink.description',
                'required' => false,
            ])
            ->add('submit', SubmitType::class, [
                'translation_domain' => 'profile',
                'label' => 'userGroup.form.labelLink.submit',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DeveloperLifeBundle\Entity\Shop\Group\UserGroup'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'developerlifebundle_shop_group_usergroup';
    }
}
