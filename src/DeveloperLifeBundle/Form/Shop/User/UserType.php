<?php

namespace DeveloperLifeBundle\Form\Shop\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UserType
 *
 * @package DeveloperLifeBundle\Form\Shop\User
 */
class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class,[
                'translation_domain' => 'shop',
                'label' => 'form.register.fieldLabel.email',
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'shop.user.plainPassword.mismatch',
                'options' => [
                    'translation_domain' => 'shop',
                ],
                'first_options' => [
                    'label' => 'form.register.fieldLabel.password',
                ],
                'second_options' => [
                    'label' => 'form.register.fieldLabel.repeatPassword',
                ],
            ])
            ->add('firstName', TextType::class,[
                'translation_domain' => 'shop',
                'label' => 'form.register.fieldLabel.firstName',
            ])
            ->add('lastName', TextType::class,[
                'translation_domain' => 'shop',
                'label' => 'form.register.fieldLabel.lastName',
            ])
            ->add('birthDate', DateType::class,[
                'translation_domain' => 'shop',
                'label' => 'form.register.fieldLabel.birthDate',
                'invalid_message' => 'shop.user.birthDate.notValid',
            ])
            ->add('submit', SubmitType::class,[
                'translation_domain' => 'shop',
                'label' => 'form.register.fieldLabel.save'
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DeveloperLifeBundle\Entity\Shop\User\User'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'developerlifebundle_shop_user_user';
    }


}
