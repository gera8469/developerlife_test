<?php

namespace DeveloperLifeBundle\Form\Shop\User\Order;

use DeveloperLifeBundle\Entity\Shop\User\DeliveryAddress;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UserOrderType
 *
 * @package DeveloperLifeBundle\Form\Shop\User\Order
 */
class UserOrderType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('deliveryAddress', EntityType::class,[
                'class' => DeliveryAddress::class,
                'translation_domain' => 'profile',
                'label' => 'userOrder.form.fieldLabel.deliveryAddress',
                'choice_label' => function (DeliveryAddress $address) {
                    return $address->getCountry() . ' ' .$address->getRegion() . ' ' . $address->getCity()
                        . ' ' . $address->getStreet() . ' ' . $address->getHouse() . '/' .  $address->getFlat();
                },
                'query_builder' => function(EntityRepository $er ) use ($options){

                    return $er->createQueryBuilder('da')
                        ->where('da.user=:user')
                        ->setParameter('user', $options['logged_user']);
                },
            ])
            ->add('submit', SubmitType::class, [
                'translation_domain' => 'profile',
                'label' => 'userOrder.form.fieldLabel.submit'
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DeveloperLifeBundle\Entity\Shop\User\Order\UserOrder'
        ));
        $resolver->setRequired('logged_user');
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'developerlifebundle_shop_user_order_userorder';
    }


}
