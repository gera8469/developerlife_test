<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 02.06.17
 * Time: 15:03
 */

namespace DeveloperLifeBundle\EventListener\Doctrine\Helper;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Trait ImageContainingTrait
 *
 * Helper for entities which contain image
 *
 * @package DeveloperLifeBundle\EventListener\Doctrine\Helper
 */
trait ImageContainingTrait
{
    /**
     * Uploaded image
     *
     * @var UploadedFile
     */
    protected $uploadImage;

    /**
     * Old image file path
     *
     * @var string
     */
    protected $oldImage;

    /**
     * @return string
     */
    public function getOldImage()
    {
        return $this->oldImage;
    }

    /**
     * @return UploadedFile
     */
    public function getUploadImage()
    {
        return $this->uploadImage;
    }

    /**
     * @param UploadedFile $uploadImage
     */
    public function setUploadImage(UploadedFile $uploadImage)
    {
        $this->uploadImage = $uploadImage;
    }

    /**
     * @return bool
     */
    public function isImageUpdated()
    {
        return !empty($this->oldImage);
    }

    /**
     * @return string|integer
     */
    abstract public function getId();
}