<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 02.06.17
 * Time: 14:06
 */

namespace DeveloperLifeBundle\EventListener\Doctrine\Product;

use DeveloperLifeBundle\Entity\Shop\Shop\Product;
use DeveloperLifeBundle\EventListener\Doctrine\Helper\ImageHandlerTrait;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ProductImageSaverListener
 *
 * Handle product`s image
 *
 * @package DeveloperLifeBundle\EventListener\Doctrine\Product
 */
class ProductImageSaverListener implements EventSubscriber
{
    use ImageHandlerTrait;

    /**
     * ProductImageSaverListener constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }


    /**
     * Save new image
     *
     * @param LifecycleEventArgs $eventArgs
     */
    public function postPersist(LifecycleEventArgs $eventArgs)
    {
        $product = $eventArgs->getObject();

        if (!$product instanceof Product || empty($product->getUploadImage())) {
            return;
        }

        $this->saveImage($product);
    }

    /**
     * Remove old image and save new image
     *
     * @param LifecycleEventArgs $eventArgs
     */
    public function postUpdate(LifecycleEventArgs $eventArgs)
    {
        $product = $eventArgs->getObject();
        if (!$product instanceof Product) {
            return;
        }

        $this->updateImage($product);
    }

    /**
     * Remove image
     *
     * @param LifecycleEventArgs $eventArgs
     */
    public function preRemove(LifecycleEventArgs $eventArgs)
    {
        $product = $eventArgs->getObject();
        if (!$product instanceof Product) {
            return;
        }

        $this->removeImage($product);
    }

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            'postPersist',
            'postUpdate',
            'preRemove'
        ];
    }

}