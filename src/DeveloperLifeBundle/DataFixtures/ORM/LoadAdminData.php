<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 30.05.17
 * Time: 18:51
 */

namespace DeveloperLifeBundle\DataFixtures\ORM;

use DeveloperLifeBundle\Entity\Admin\Admin;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class LoadAdminData
 *
 * @package DeveloperLifeBundle\DataFixtures\ORM
 */
class LoadAdminData extends AbstractFixture implements
    OrderedFixtureInterface,
    ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $admin = new Admin();
        $admin->setEmail('gera8469@gmail.com');
        $admin->setNickname('admin');
        $admin->setPassword($this->container->get('security.password_encoder')
            ->encodePassword($admin, 'admin')
        );
        $manager->persist($admin);
        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 1;
    }
}