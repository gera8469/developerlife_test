<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 30.05.17
 * Time: 21:53
 */

namespace DeveloperLifeBundle\DataFixtures\ORM;

use DeveloperLifeBundle\DataFixtures\FixtureHelper;
use DeveloperLifeBundle\Entity\Shop\User\DeliveryAddress;
use DeveloperLifeBundle\Entity\Shop\User\User;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUserData extends FixtureHelper
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $user1 = new User();
        $user1->setFirstName('Владимир');
        $user1->setLastName('Геращенко');
        $user1->setEmail('gera8469@gmail.com');
        $user1->setBirthDate(new \DateTime('1987-10-24'));
        $user1->setPassword($this->container->get('security.password_encoder')
            ->encodePassword($user1, 'password')
        );
        $user1->setRegistrationDate(new \DateTime());
        $manager->persist($user1);

        $deliveryAddress1 = new DeliveryAddress();
        $deliveryAddress1->setUser($user1);
        $deliveryAddress1->setCountry('Украина');
        $deliveryAddress1->setRegion('Сумская');
        $deliveryAddress1->setCity('Сумы');
        $deliveryAddress1->setStreet('Прокофьева');
        $deliveryAddress1->setHouse('13');
        $deliveryAddress1->setFlat('77');
        $manager->persist($deliveryAddress1);

        $user2 = new User();
        $user2->setFirstName('Сергей');
        $user2->setLastName('Геращенко');
        $user2->setEmail('gera8470@gmail.com');
        $user2->setBirthDate(new \DateTime('1992-06-17'));
        $user2->setPassword($this->container->get('security.password_encoder')
            ->encodePassword($user2, 'password')
        );
        $user2->setRegistrationDate(new \DateTime());
        $manager->persist($user2);

        $deliveryAddress2 = new DeliveryAddress();
        $deliveryAddress2->setUser($user2);
        $deliveryAddress2->setCountry('Украина');
        $deliveryAddress2->setRegion('Сумская');
        $deliveryAddress2->setCity('Сумы');
        $deliveryAddress2->setStreet('Прокофьева');
        $deliveryAddress2->setHouse('13');
        $deliveryAddress2->setFlat('88');
        $manager->persist($deliveryAddress2);

        $manager->flush();

        $this->setReference('userVladimir', $user1);

    }



    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 2;
    }
}