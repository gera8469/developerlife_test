<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 07.05.17
 * Time: 15:09
 */

namespace DeveloperLifeBundle\DataFixtures;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class FixtureHelper
 *
 * Helper for fixture handling
 *
 * @package DeveloperLifeBundle\DataFixtures
 */
abstract class FixtureHelper extends AbstractFixture implements
    ContainerAwareInterface,
    OrderedFixtureInterface
{
    use ContainerAwareTrait;
    /**
     * @param ObjectManager $manager
     * @param array $entityData
     * @param string $entityClassName
     *
     * @return Collection
     */
    protected function handleFixtureData(ObjectManager $manager, array $entityData, $entityClassName)
    {
        $entityCollection = $this->createObjects($entityData, $entityClassName);

        if (!$entityCollection->isEmpty()) {
            $this->saveEntityObjects($manager, $entityCollection);
        }

        return $entityCollection;
    }
    /**
     * Return collection of objects based on $entityData array
     *
     * @param array $entityData
     * @param string $entityClassName
     * @return Collection|false
     */
    private function createObjects(array $entityData, $entityClassName)
    {
        $entityCollection = new ArrayCollection();
        foreach ($entityData as $objectAlias => $item) {
            $entityObject = new $entityClassName();
            foreach ($item as $key => $value) {
                $methodName = 'set' . ucfirst($key);

                $entityObject->{$methodName}($value);
            }
            $entityCollection[$objectAlias] = $entityObject;
        }

        return !$entityCollection->isEmpty() ? $entityCollection : false;
    }

    /**
     * Save entity objects to the database
     *
     * @param ObjectManager $manager
     * @param Collection $entityCollection
     *
     * @return Collection
     */
    private function saveEntityObjects(ObjectManager $manager, Collection $entityCollection)
    {
        foreach ($entityCollection as $entityObject) {
            $manager->persist($entityObject);
        }
        $manager->flush();
    }
}