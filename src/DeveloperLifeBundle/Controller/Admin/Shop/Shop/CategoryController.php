<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 30.05.17
 * Time: 14:58
 */

namespace DeveloperLifeBundle\Controller\Admin\Shop\Shop;

use DeveloperLifeBundle\Entity\Shop\Shop\Category;
use DeveloperLifeBundle\Form\Admin\Shop\Shop\CategoryType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class CategoryController
 *
 * Handle category
 *
 * @package DeveloperLifeBundle\Controller\Admin\Shop\Shop
 */
class CategoryController extends Controller
{
    /**
     * Category list
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $repository = $this->getCategoryRepository();

        return $this->render('@DeveloperLife/admin/shop/category/indexCategory.html.twig',[
            'categories' => $repository->findAll(),
        ]);
    }

    /**
     * New category
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category,[
            'action' => $this->generateUrl('admin_shop_category_new')
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            return $this->redirectToRoute('admin_shop_category_show', [
                'id' => $category->getId(),
            ]);
        }

        return $this->render('@DeveloperLife/admin/shop/category/newCategory.html.twig',[
            'form' => $form->createView(),
        ]);
    }

    /**
     * Show category
     *
     * @throws NotFoundHttpException
     * @param integer $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($id)
    {
        $repository = $this->getCategoryRepository();
        $category = $repository->find($id);

        if (!$category) {
            throw $this->createNotFoundException();
        }

        return $this->render('@DeveloperLife/admin/shop/category/showCategory.html.twig',[
            'category' => $category
        ]);
    }

    /**
     * Edit category
     *
     * @throws NotFoundHttpException
     * @param integer $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction($id, Request $request)
    {
        $repository = $this->getCategoryRepository();
        $category = $repository->find($id);

        if (!$category) {
            throw $this->createNotFoundException();
        }

        $form = $this->createForm(CategoryType::class, $category,[
            'action' => $this->generateUrl('admin_shop_category_edit', [
                'id' => $category->getId(),
            ]),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            return $this->redirectToRoute('admin_shop_category_show', [
                'id' => $category->getId(),
            ]);
        }

        return $this->render('@DeveloperLife/admin/shop/category/newCategory.html.twig',[
            'form' => $form->createView(),
        ]);
    }

    /**
     * Remove category
     *
     * @throws NotFoundHttpException
     * @param integer $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeAction($id)
    {
        $repository = $this->getCategoryRepository();
        $category = $repository->find($id);
        $em = $this->getDoctrine()->getManager();

        if (!$category) {
            throw $this->createNotFoundException();
        }

        $em->remove($category);
        $em->flush();

        return $this->redirectToRoute('admin_shop_category');
    }

    /**
     * Return CategoryRepository
     *
     * @return \DeveloperLifeBundle\Repository\Shop\Shop\CategoryRepository
     */
    private function getCategoryRepository()
    {
        return $this->getDoctrine()->getRepository(Category::class);

    }
}