<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 11.06.17
 * Time: 14:04
 */

namespace DeveloperLifeBundle\Controller\Admin\Shop\Order;

use DeveloperLifeBundle\Entity\Shop\Group\Order\GroupOrder;
use DeveloperLifeBundle\Entity\Shop\Group\Order\GroupOrderHasProduct;
use DeveloperLifeBundle\Entity\Shop\User\Order\UserOrder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class OrderController
 *
 * Handle order
 *
 * @package DeveloperLifeBundle\Controller\Admin\Shop\Order
 */
class OrderController extends Controller
{
    /**
     * User and group order lists
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return $this->render('@DeveloperLife/admin/shop/order/indexOrder.html.twig', [
            'userOrders' => $this->getDoctrine()->getRepository(UserOrder::class)->findAll(),
            'groupOrders' => $this->getDoctrine()->getRepository(GroupOrder::class)->findAll(),
        ]);
    }

    /**
     * Show user order
     *
     * @param integer $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showUserOrderAction($id)
    {
        $userOrder = $this->getDoctrine()->getRepository(UserOrder::class)->find($id);

        if (!$userOrder) {
            throw $this->createNotFoundException();
        }

        return $this->render('@DeveloperLife/admin/shop/order/show/showUserOrder.html.twig', [
            'userOrder' => $userOrder,
        ]);
    }

    /**
     * Show group order
     *
     * @throws NotFoundHttpException
     * @param integer $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showGroupOrderAction($id)
    {
        $groupOrder = $this->getDoctrine()->getRepository(GroupOrder::class)->find($id);

        if (!$groupOrder) {
            throw $this->createNotFoundException();
        }

        $orderData = [];

        /* @var $groupOrderHasProduct GroupOrderHasProduct */
        foreach ($groupOrder->getGroupOrderHasProducts() as $groupOrderHasProduct) {
            $user = $groupOrderHasProduct->getUserHasGroup()->getUser();
            $product = $groupOrderHasProduct->getProduct();

            $orderData[$user->getId()]['user'] = $user;
            $orderData[$user->getId()]['products'][] = [
                'product' => $product,
                'price' => $groupOrderHasProduct->getPrice(),
                'quantity' => $groupOrderHasProduct->getQuantity(),
            ];

            if (empty($orderData[$user->getId()]['summaryData'])) {
                $orderData[$user->getId()]['summaryData'] = [
                    'summaryQuantity' => 0,
                    'summaryCost' => 0
                ];
            }

            $orderData[$user->getId()]['summaryData']['summaryQuantity'] += $groupOrderHasProduct->getQuantity();
            $orderData[$user->getId()]['summaryData']['summaryCost'] += $groupOrderHasProduct->getPrice();
        }

        return $this->render('@DeveloperLife/admin/shop/order/show/showGroupOrder.html.twig', [
            'groupOrder' => $groupOrder,
            'orderdata' => $orderData,
        ]);
    }
}