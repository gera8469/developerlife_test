<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 11.06.17
 * Time: 11:09
 */

namespace DeveloperLifeBundle\Controller\Admin\Shop\User;

use DeveloperLifeBundle\Entity\Shop\User\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class UserController
 *
 * Handle user
 *
 * @package DeveloperLifeBundle\Controller\Admin\Shop\User
 */
class UserController extends Controller
{
    /**
     * User list
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return $this->render('@DeveloperLife/admin/shop/user/indexUser.html.twig', [
            'users' => $this->getDoctrine()->getRepository(User::class)->findAll(),
        ]);
    }

    /**
     * Show user
     *
     * @throws  NotFoundHttpException
     * @param integer $id
     * @param string $infoAlias Information alias
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($id, $infoAlias)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);

        if (!$user) {
            throw $this->createNotFoundException();
        }

        return $this->render('@DeveloperLife/admin/shop/user/showUser.html.twig', [
            'user' => $user,
            'infoAlias' => $infoAlias,
        ]);
    }
}