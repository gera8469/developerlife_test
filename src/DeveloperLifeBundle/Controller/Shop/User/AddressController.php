<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 06.06.17
 * Time: 11:10
 */

namespace DeveloperLifeBundle\Controller\Shop\User;

use DeveloperLifeBundle\Entity\Shop\User\DeliveryAddress;
use DeveloperLifeBundle\Form\Shop\User\DeliveryAddressType;
use DeveloperLifeBundle\Repository\Shop\User\DeliveryAddressRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class AddressController
 *
 * Handle delivery address
 *
 * @package DeveloperLifeBundle\Controller\Shop\User
 */
class AddressController extends Controller
{
    /**
     * Delivery address list
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return $this->render('@DeveloperLife/user/address/indexDeliveryAddress.html.twig',[
                'addresses' => $this->getDeliveryAddressRepository()->findBy([
                'user' => $this->getUser()
            ])
        ]);
    }

    /**
     * New delivery address
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $deliveryAddress = new DeliveryAddress();

        $form = $this->createForm(DeliveryAddressType::class, $deliveryAddress,[
            'action' => $this->generateUrl('shop_profile_address_new')
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $deliveryAddress->setUser($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($deliveryAddress);
            $em->flush();

            return $this->redirectToRoute('shop_profile_address');
        }

        return $this->render('@DeveloperLife/user/address/addressForm.html.twig',[
            'form' => $form->createView(),
        ]);
    }

    /**
     * Edit delivery address
     *
     * @throws NotFoundHttpException
     * @param integer $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction($id, Request $request)
    {
        $repository = $this->getDeliveryAddressRepository();
        $deliveryAddress = $repository->findOneBy([
            'id' => $id,
            'user' => $this->getUser(),
        ]);

        if (!$deliveryAddress) {
            throw $this->createNotFoundException();
        }

        $form = $this->createForm(DeliveryAddressType::class, $deliveryAddress,[
            'action' => $this->generateUrl('shop_profile_address_edit', [
                'id' => $deliveryAddress->getId(),
            ]),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($deliveryAddress);
            $em->flush();

            return $this->redirectToRoute('shop_profile_address');
        }

        return $this->render('@DeveloperLife/user/address/addressForm.html.twig',[
            'form' => $form->createView(),
        ]);
    }

    /**
     * Remove delivery address
     *
     * @throws NotFoundHttpException
     * @param integer $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeAction($id)
    {
        $repository = $this->getDeliveryAddressRepository();
        $deliveryAddress = $repository->findOneBy([
            'id' => $id,
            'user' => $this->getUser(),
        ]);
        $em = $this->getDoctrine()->getManager();

        if (!$deliveryAddress) {
            throw $this->createNotFoundException();
        }

        $em->remove($deliveryAddress);
        $em->flush();

        return $this->redirectToRoute('shop_profile_address');
    }

    /**
     * Return DeliveryAddressRepository
     *
     * @return DeliveryAddressRepository
     */
    private function getDeliveryAddressRepository()
    {
        return $this->getDoctrine()->getRepository(DeliveryAddress::class);
    }


}