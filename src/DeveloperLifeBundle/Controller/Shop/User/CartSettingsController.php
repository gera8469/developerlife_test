<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 08.06.17
 * Time: 19:18
 */

namespace DeveloperLifeBundle\Controller\Shop\User;

use DeveloperLifeBundle\Entity\Shop\Group\Cart\CartMode;
use DeveloperLifeBundle\Entity\Shop\Group\UserGroup;
use DeveloperLifeBundle\Entity\Shop\Group\UserHasGroup;
use DeveloperLifeBundle\Helper\Cart\Base\AbstractCartHandler;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class CartSettingsController
 *
 * Handle cart settings
 *
 * @package DeveloperLifeBundle\Controller\Shop\User
 */
class CartSettingsController extends Controller
{
    /**
     * Cart settings
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $cartSettings = AbstractCartHandler::getCartSettings($this->get('service_container'));
        /* @var $userHasGroups UserHasGroup[] */
        $userHasGroups = $this->getUser()->getUserHasGroups();

        $response = $this->render('@DeveloperLife/user/cart/indexCart.html.twig',[
            'cartModes' => $this->getDoctrine()->getRepository(CartMode::class)->findAll(),
            'activeCartMode' => $cartSettings['cartMode'],
            'activeGroup' => $cartSettings['activeGroup'],
            'userHasGroups' => $userHasGroups,
            'action' => $this->generateUrl('shop_profile_cart_mode_switch')
        ]);

        $response->headers->setCookie(new Cookie('cartSettings', json_encode($cartSettings),
            AbstractCartHandler::CART_LIFETIME));

        return $response;
    }

    /**
     * Switch cart mode (user cart|group cart)
     *
     * @throws NotFoundHttpException
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function switchModeAction(Request $request)
    {
        $doctrine = $this->getDoctrine();

        $cartMode = $doctrine->getRepository(CartMode::class)->find((int)$request->get('cart')['mode']);
        $userGroup = $doctrine->getRepository(UserGroup::class)->find((int)$request->get('cart')['active_group']);


        if (!$cartMode || !$userGroup) {
            throw $this->createNotFoundException();
        }

        $cartSettings = AbstractCartHandler::getCartSettings($this->get('service_container'));
        $cartSettings['cartMode'] = $cartMode->getAlias();

        if ($cartMode->getAlias() == 'groupCart') {
            $cartSettings['activeGroup'] = $userGroup->getId();
        }
        $response = $this->redirectToRoute('shop_profile_cart_settings');
        $response->headers->setCookie(new Cookie('cartSettings', json_encode($cartSettings),
            AbstractCartHandler::CART_LIFETIME));

        return $response;
    }
}