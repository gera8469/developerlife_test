<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 04.06.17
 * Time: 11:09
 */

namespace DeveloperLifeBundle\Controller\Shop\Shop;

use DeveloperLifeBundle\Entity\Shop\Shop\Category;
use DeveloperLifeBundle\Entity\Shop\Shop\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ProductController
 *
 * Handle product
 *
 * @package DeveloperLifeBundle\Controller\Shop\Shop
 */
class ProductController extends Controller
{
    /**
     * Get products by category
     *
     * @throws NotFoundHttpException
     * @param integer $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getProductsByCategoryAction($id)
    {
        if (!$category = $this->getDoctrine()->getRepository(Category::class)->find($id)) {
            throw $this->createNotFoundException();
        }

        return $this->render('@DeveloperLife/shop/shop/category/allProductsByCategory.html.twig',[
            'category' => $category,
        ]);
    }

    /**
     * Show product
     *
     * @throws NotFoundHttpException
     * @param integer $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($id)
    {
        $doctrine = $this->getDoctrine();
        $product = $doctrine->getRepository(Product::class)->find($id);

        if (!$product) {
            throw $this->createNotFoundException();
        }

        return $this->render('@DeveloperLife/shop/shop/product/showProduct.html.twig', [
            'product' => $product,
        ]);
    }

    /**
     * Category menu (left sidebar)
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getSideBarAction()
    {
        return $this->render('@DeveloperLife/shop/shop/category/partial/sideBar.html.twig', [
            'categories' => $this->getDoctrine()->getRepository(Category::class)->findAll(),
        ]);
    }
}