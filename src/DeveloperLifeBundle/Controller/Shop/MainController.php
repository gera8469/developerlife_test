<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 03.06.17
 * Time: 21:18
 */

namespace DeveloperLifeBundle\Controller\Shop;

use DeveloperLifeBundle\Entity\Shop\Shop\Category;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class MainController
 *
 * @package DeveloperLifeBundle\Controller\Shop
 */
class MainController extends Controller
{
    /**
     * Home page
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return $this->render('@DeveloperLife/layouts/shopLayout.html.twig',[
            'categories' => $this->getDoctrine()->getRepository(Category::class)->findAll(),
        ]);
    }

    /**
     * Profile index page
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function profileIndexAction()
    {
        return $this->render('@DeveloperLife/user/profile/indexProfile.html.twig', [
            'categories' => $this->getDoctrine()->getRepository(Category::class)->findAll(),
        ]);
    }
}