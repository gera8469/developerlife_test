<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170603174508 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE UNIQUE INDEX unique_userhasgroup_product ON group_cart (user_has_group_id, product_id)');
        $this->addSql('CREATE UNIQUE INDEX unique_user_usergroup ON user_has_group (user_id, user_group_id)');
        $this->addSql('CREATE UNIQUE INDEX unique_userorder_product ON user_order_has_product (user_order_id, product_id)');
        $this->addSql('CREATE UNIQUE INDEX unique_userhasgroup_product_grouporder ON group_order_has_product (user_has_group_id, product_id, group_order_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX unique_userhasgroup_product ON group_cart');
        $this->addSql('DROP INDEX unique_userhasgroup_product_grouporder ON group_order_has_product');
        $this->addSql('DROP INDEX unique_user_usergroup ON user_has_group');
        $this->addSql('DROP INDEX unique_userorder_product ON user_order_has_product');
    }
}
