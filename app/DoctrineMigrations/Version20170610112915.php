<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170610112915 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE group_order ADD delivery_address_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE group_order ADD CONSTRAINT FK_A505B8FFEBF23851 FOREIGN KEY (delivery_address_id) REFERENCES delivery_address (id)');
        $this->addSql('CREATE INDEX IDX_A505B8FFEBF23851 ON group_order (delivery_address_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE group_order DROP FOREIGN KEY FK_A505B8FFEBF23851');
        $this->addSql('DROP INDEX IDX_A505B8FFEBF23851 ON group_order');
        $this->addSql('ALTER TABLE group_order DROP delivery_address_id');
    }
}
