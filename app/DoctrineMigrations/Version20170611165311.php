<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170611165311 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE group_order_has_product DROP FOREIGN KEY FK_714D3CCC6FFE5F71');
        $this->addSql('ALTER TABLE group_order_has_product ADD CONSTRAINT FK_714D3CCC6FFE5F71 FOREIGN KEY (user_has_group_id) REFERENCES user_has_group (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE group_order_has_product DROP FOREIGN KEY FK_714D3CCC6FFE5F71');
        $this->addSql('ALTER TABLE group_order_has_product ADD CONSTRAINT FK_714D3CCC6FFE5F71 FOREIGN KEY (user_has_group_id) REFERENCES user_has_group (id)');
    }
}
