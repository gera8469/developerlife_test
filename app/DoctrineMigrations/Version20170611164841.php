<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170611164841 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_has_group DROP FOREIGN KEY FK_96A9D99F1ED93D47');
        $this->addSql('ALTER TABLE user_has_group ADD CONSTRAINT FK_96A9D99F1ED93D47 FOREIGN KEY (user_group_id) REFERENCES user_group (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE join_request DROP FOREIGN KEY FK_E932E4FF1ED93D47');
        $this->addSql('ALTER TABLE join_request ADD CONSTRAINT FK_E932E4FF1ED93D47 FOREIGN KEY (user_group_id) REFERENCES user_group (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE group_order DROP FOREIGN KEY FK_A505B8FF1ED93D47');
        $this->addSql('ALTER TABLE group_order ADD CONSTRAINT FK_A505B8FF1ED93D47 FOREIGN KEY (user_group_id) REFERENCES user_group (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE group_order DROP FOREIGN KEY FK_A505B8FF1ED93D47');
        $this->addSql('ALTER TABLE group_order ADD CONSTRAINT FK_A505B8FF1ED93D47 FOREIGN KEY (user_group_id) REFERENCES user_group (id)');
        $this->addSql('ALTER TABLE join_request DROP FOREIGN KEY FK_E932E4FF1ED93D47');
        $this->addSql('ALTER TABLE join_request ADD CONSTRAINT FK_E932E4FF1ED93D47 FOREIGN KEY (user_group_id) REFERENCES user_group (id)');
        $this->addSql('ALTER TABLE user_has_group DROP FOREIGN KEY FK_96A9D99F1ED93D47');
        $this->addSql('ALTER TABLE user_has_group ADD CONSTRAINT FK_96A9D99F1ED93D47 FOREIGN KEY (user_group_id) REFERENCES user_group (id)');
    }
}
